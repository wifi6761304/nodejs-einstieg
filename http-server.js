const http = require('http')
const moment = require('moment')

/*
	Ein Webserver, der Antwort auf http Requests gibt.
	Als Parameter wird ein callback erwartet. Dieser Callback
	erhält den Request als Objekt und erlaubt es, die Antwort
	(Response), zu konfigurieren und zurückzugeben.
*/
const server = http.createServer((req, res) => {
	// Datum als Y-m-d H:i:s
	const date = new Date()
	// Same as Date default
	const dateFormated = moment().format("Y-MM-DD H:mm:ss")

	// Set status code
	res.statusCode = 200
	// set more headers. Achtung, headers müssen gesetzt werden,
	// bevor es zu einer Ausgabe kommt!
	res.setHeader("Content-type", "text/html")
	// Alternative
	// res.writeHead(200, {
	// 	"Content-type": "text/plain"
	// })
	// Wir antworten!
	res.end(`<p>Es ist Zeit: ${dateFormated}</p>`)
})

// Das Server Objekt aktivieren. Wir lauschen an einer Adresse und einem Port
// auf http requests.
const port = 3000
const host = "127.0.0.1"

server.listen(port, host, () => {
	console.log(`Server erreichbar unter http://${host}:${port}`)
})