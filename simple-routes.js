const http = require("http")
// Alternative ES6 Module import syntax -> in package.json Attribut "type": "module" setzen
// import http from "http"

const server = http.createServer((req, res) => {
	// home page -> /, shop -> /shop, impressum -> /imprint
	// h1 mit Welcome Home, Welcome shop, Welcome imprint
	const url = req.url
	let content = "";

	// Als HTML Datei identifizieren - dies geschieht über den http header
	res.writeHead(200, {
		"Content-type": "text/html"
	})
	/*
		Wenn aus einer endlichen Anzahl an Optionen gewählt werden
		muss, ist switch oft besser und leserlicher als if ... else if ... else if
		break beendet den jeweiligen case. Wird break nicht geschrieben, wird auch der
		nächste case ausgeführt.
	*/
	switch (url) {
		case "/":
			content = "<h1>Welcome Home!</h1>"
			break;
		case "/shop":
			content = "<h1>Welcome to our shop!</h1>"
			break;
		case "/imprint":
			content = "<h1>This is our imprint page</h1>"
			break;
		// Wird ausgeführt, wenn keiner der anderen Fälle zutrifft.
		default:
			// TODO: header auf 404 setzen
			content = "<h1>Page not found!</h1>"
	}

	res.end(content)
})

const port = 3000
const host = "127.0.0.1"

server.listen(port, host, () => {
	console.log(`Server erreichbar unter http://${host}:${port}`)
})